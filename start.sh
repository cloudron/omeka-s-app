#!/bin/bash

set -eu

echo "=> Start Omeka S"
ldap="${OMEKA_S_LDAP_VERSION}"

export MYSQL_PWD=${CLOUDRON_MYSQL_PASSWORD} # This will avoid the mysql password warning
mysql="mysql --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE}"

mkdir -p /run/php/sessions

if [[ ! -d "/app/data/files" ]]; then
    cp -rf /app/code/files_vanilla /app/data/files
fi

if [[ ! -d "/app/data/themes" ]]; then
    mkdir /app/data/themes ;
    ln -sf /app/code/themes_vanilla/default /app/data/themes/default
fi

if [[ ! -d "/app/data/modules" ]]; then
    mkdir /app/data/modules ;
fi

if [[ -d "/app/data/modules/Ldap" ]]; then
    rm -Rf /app/data/modules/Ldap
fi

if [[ ! -L "/app/data/modules/Ldap" ]]; then
    ln -sf /app/code/modules_vanilla/Ldap /app/data/modules/Ldap
fi

if [[ ! -f "/app/data/config/local.config.php" ]]; then
    mkdir /app/data/config ;
    cp /app/code/config/local.config_vanilla.php /app/data/config/local.config.php
fi

if [[ ! -f "/app/data/.htaccess" ]]; then
    cp /app/code/htaccess /app/data/.htaccess
fi

# workaroud until fixed upstream, see .htaccess in package
sed '/^SetEnv APPLICATION_ENV/d' -i /app/data/.htaccess

chown -R www-data:www-data /app/data /run/php/sessions

if [[ ! -f /app/data/.initialized ]]; then
    echo "=> Starting apache for setup Omeka S first user"
    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    ( /usr/sbin/apache2 -DFOREGROUND ) &
    apache_pid=$!

    sleep 1

    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "==> Waiting for apache2 to start"
        sleep 1
    done
    curl "http://localhost:3000/install" \
        --data-urlencode "user[email]=admin@example.com" \
        --data-urlencode "user[email-confirm]=admin@example.com" \
        --data-urlencode "user[name]=admin" \
        --data-urlencode "user[password-confirm][password]=changeme" \
        --data-urlencode "user[password-confirm][password-confirm]=changeme" \
        --data-urlencode "settings[installation_title]=Omeka S network" \
        --data-urlencode "settings[time_zone]=UTC" \
        --data-urlencode "settings[locale]="

    touch /app/data/.initialized

    echo "=> Setup Omeka S complete"
    kill ${apache_pid}

    echo "=> Set sender email"

    $mysql <<MYSQL_INPUT 
    UPDATE setting 
    SET value=REPLACE(value, 'admin@example.com', '${CLOUDRON_MAIL_FROM}')
        WHERE id='administrator_email'; 
    INSERT INTO module (id, is_active, version) VALUES ('Ldap', 1, '$ldap');
    INSERT INTO setting (id, value) VALUES 
        ('ldap_role', '\"researcher\"'), 
        ('ldap_email_attribute', '\"mail\"'), 
        ('ldap_name_attribute', '\"username\"');
MYSQL_INPUT

else

$mysql <<MYSQL_INPUT
UPDATE module SET version='$ldap' WHERE id="Ldap";
MYSQL_INPUT

    echo "=> Starting apache for database migration"
    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    ( /usr/sbin/apache2 -DFOREGROUND ) &
    apache_pid=$!

    sleep 1

    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "==> Waiting for apache2 to start"
        sleep 1
    done

    echo "=> Run database migration"
    curl -X POST "http://localhost:3000/migrate"

    # migration done, stop this apache
    kill ${apache_pid}
fi

# Give apache some time to shut down
sleep 2

echo "=> Start main apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
