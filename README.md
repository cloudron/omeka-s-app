# Omeka S Cloudron app

This repository contains the Cloudron app package source for [Omeka S](https://omeka.org/s/).

This app is experimental and needs to be extensively tested.

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/documentation/custom-apps/tutorial/#cloudron-cli).

```
cd omeka-s-app

cloudron build
cloudron install
```


## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the posts are still ok.

```
cd omeka-s-app/test

npm install
USERNAME=<username> PASSWORD=<password> node_modules/.bin/mocha --bail test.js
```
