FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y libvips-tools poppler-utils catdoc docx2txt lynx odt2txt php-solr && rm -rf /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-releases depName=omeka/omeka-s versioning=semver extractVersion=^v(?<version>.+)$
ARG OMEKA_S_VERSION=4.1.1

# download and install omeka-s
RUN wget https://github.com/omeka/omeka-s/releases/download/v${OMEKA_S_VERSION}/omeka-s-${OMEKA_S_VERSION}.zip \
    -O /tmp/omeka-s.zip && \
    unzip /tmp/omeka-s.zip -d /tmp

RUN mv /tmp/omeka-s/files /tmp/omeka-s/files_vanilla
RUN mv /tmp/omeka-s/themes /tmp/omeka-s/themes_vanilla
RUN mv /tmp/omeka-s/modules /tmp/omeka-s/modules_vanilla

# download and install Ldap module
# renovate: datasource=github-releases depName=biblibre/omeka-s-module-Ldap versioning=semver
ENV OMEKA_S_LDAP_VERSION=0.5.0

RUN wget https://github.com/biblibre/omeka-s-module-Ldap/releases/download/v${OMEKA_S_LDAP_VERSION}/Ldap-${OMEKA_S_LDAP_VERSION}.zip \
    -O /tmp/omeka-s/modules_vanilla/Ldap.zip && \
    unzip /tmp/omeka-s/modules_vanilla/Ldap.zip -d /tmp/omeka-s/modules_vanilla/ && \
    rm /tmp/omeka-s/modules_vanilla/Ldap.zip
RUN cd /tmp/omeka-s/modules_vanilla/Ldap/ && composer install --no-dev

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log && a2dismod perl && a2enmod headers rewrite env
COPY apache/app.conf /etc/apache2/sites-enabled/app.conf
RUN echo "Listen 3000" > /etc/apache2/ports.conf

# configure mod_php
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/php/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

# move files and folders
RUN rsync -r /tmp/omeka-s/ /app/code/
RUN rm -Rf /tmp/omeka-s* /app/code/.htaccess

COPY database.ini /app/code/config/
COPY local.config.php /app/code/config/local.config_vanilla.php
COPY .htaccess /app/code/htaccess

RUN rm /app/code/config/local.config.php
RUN ln -sf /app/data/config/local.config.php /app/code/config/local.config.php
RUN ln -sf /app/data/.htaccess /app/code/.htaccess
RUN ln -sf /app/data/files /app/code/files
RUN ln -sf /app/data/themes /app/code/themes
RUN ln -sf /app/data/modules /app/code/modules

RUN chown -R www-data.www-data /app/code

# add code
COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
