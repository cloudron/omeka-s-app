Omeka S is pre-setup with an admin account. The initial credentials are:

**Email**: admin@example.com<br/>
**Password**: changeme<br/>

**Note:** Please change the email and the password upon first login. 

<sso>Cloudron users can login with their username. They are assigned
the `researcher` role by default.</sso>

