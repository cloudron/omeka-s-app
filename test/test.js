#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL || !process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, EMAIL and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EMAIL = 'admin@example.com';
    const PASSWORD = 'changeme';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const MANIFEST = require('../CloudronManifest.json');
    const LDAP_USER_ROLE = 'Researcher';
    const LDAP_USER_MAIL = process.env.EMAIL;
    const LDAP_USER_PASSWORD = process.env.PASSWORD;
    const LDAP_USER_USERNAME = process.env.USERNAME;

    var browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login() {
        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(EMAIL);
        await browser.findElement(By.id('password')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//input[@type="submit"]')).click();
        await waitForElement(By.xpath('//div/h1/span[text()="Admin dashboard"]'));
    }

    async function ldapLogin() {
        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.id('email'));
        await browser.findElement(By.id('email')).sendKeys(LDAP_USER_USERNAME);
        await browser.findElement(By.id('password')).sendKeys(LDAP_USER_PASSWORD);
        await browser.findElement(By.xpath('//input[@type="submit"]')).click();
        await waitForElement(By.xpath('//div/h1/span[text()="Admin dashboard"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/admin`);
        await waitForElement(By.xpath('//div/h1/span[text()="Admin dashboard"]'));
        await browser.findElement(By.xpath('//a[text()="Logout"]')).click();
        await waitForElement(By.id('password'));
    }

    async function ldapEnabled() {
        await browser.get(`https://${app.fqdn}/admin/module`);
        await waitForElement(By.xpath('//div/h1/span[text()="Modules"]'));
        await browser.findElement(By.xpath('//a[text()="Configure"]')).click();
        await waitForElement(By.xpath(`//select[@id="ldap_role"]/option[text()="${LDAP_USER_ROLE}"]`));
    }

    async function createItem() {
        await browser.get(`https://${app.fqdn}/admin/item`);
        await waitForElement(By.xpath('//div/h1/span[text()="Items"]'));
        await browser.findElement(By.xpath('//a[text()="Add new item"]')).click();
        await waitForElement(By.xpath('//div/h1/span[text()="New item"]'));
        await browser.findElement(By.xpath('//div[@data-property-term="dcterms:title"]//textarea')).sendKeys(TITLE);
        await browser.findElement(By.xpath('//div[@data-property-term="dcterms:description"]//textarea')).sendKeys(DESCRIPTION);
        await browser.findElement(By.xpath('//button[@name="add-item-submit"]')).click();
        await waitForElement(By.id('content'));
    }

    async function itemExists() {
        await browser.get(`https://${app.fqdn}/admin/item`);
        await waitForElement(By.xpath(`//a[@class="resource-link"]//span[text()="${TITLE}"]`));
        await browser.findElement(By.xpath(`//a[@class="resource-link"]//span[text()="${TITLE}"]`)).click();
        await waitForElement(By.xpath(`//div/h1/span[text()="${TITLE}"]`));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('Ldap enabled ', ldapEnabled);
    it('can create item', createItem);
    it('can logout', logout);

    it('can ldap login', ldapLogin);
    it('item exists', itemExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('Ldap enabled ', ldapEnabled);
    it('item exists', itemExists);
    it('can logout', logout);

    it('can ldap login', ldapLogin);
    it('item exists', itemExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('Ldap enabled ', ldapEnabled);
    it('item exists', itemExists);
    it('can logout', logout);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('Ldap enabled ', ldapEnabled);
    it('item exists', itemExists);
    it('can logout', logout);

    it('can ldap login', ldapLogin);
    it('item exists', itemExists);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id ${MANIFEST.id} --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create item', createItem);
    it('item exists', itemExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('item exists', itemExists);
    it('can logout', logout);

    it('can ldap login', ldapLogin);
    it('item exists', itemExists);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
