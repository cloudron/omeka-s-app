### Overview

Omeka S is a next-generation web publishing platform for communities and institutions interested in connecting digital cultural heritage collections with other resources online. It's a libre software that allows researchers, universities, galleries, libraries, archives, museums, etc. to manage their data and resources and creates a local network of independently curated exhibits sharing a collaboratively built pool of items and their metadata. 
 
### Features 

- **Install Once** Create and manage many sites with a streamlined installation of Omeka S.
- **Connect to the Semantic Web** Publish items with linked open data.
- **Share with DPLA** Describe items with DPLA-ready resource templates.
- **Extend and Build** Extend functionality of Omeka S sites with modules to map, collect, import, and connect resources.
- **Design with Ease** Style each Omeka S site with a different fully-responsive theme to fit any screen size.

### Installation information

- Cloudron user-management is supported by the pre-installed Ldap module where user email and name attributes settings with `mail` and `username`. The default user role is "researcher", the lower role. Please note that users who connect this way must login with their username and not their email address.
- You can extend the functionality of Omeka S by installing manually in the directory `/app/data/modules` with Cloudron CLI or file manager UI. 
- Omeka S comes with a default theme. You can install other themes in `/app/data/themes` directory. 

